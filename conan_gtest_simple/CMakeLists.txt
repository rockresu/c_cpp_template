cmake_minimum_required(VERSION 3.22)
project(conan_gtest_simple)

set(CMAKE_CXX_STANDARD 17)
set(CONAN_DISABLE_CHECK_COMPILER TRUE)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_library(library
        src/example.c
        src/dependency_injection.c
        src/singleton.c)
target_include_directories(library PUBLIC src)

add_subdirectory(tests)