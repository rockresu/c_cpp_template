#include <malloc.h>
#include "dependency_injection.h"

typedef struct __DependencyInjection {
    const Runnable* runnable;
} DependencyInjection;

// Inject a generic implementation to the class.
// The class can now use the runnable without having to know anything about the implementation.
DependencyInjectionHandle DependencyInjection_create(const Runnable* runnable) {
    DependencyInjectionHandle me = malloc(sizeof(DependencyInjection));
    me->runnable = runnable;
    return me;
}
