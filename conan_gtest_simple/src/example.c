#include <malloc.h>
#include "example.h"

// Private data
typedef struct __Class {
    Runnable runnable;
    size_t increment;
} Class;

static void run(Runnable* handle);

// ,Implement the behavior
static void run(Runnable* handle) {
    // Cast the handle to the correct implementing object
    Class* me = (Class*) handle->handle;
    me->increment += 1;
}

// Create a new instance
Class* Class_create() {
    // Allocate the new class. This can also be done on a static heap.
    Class* me = malloc(sizeof(Class));
    // Assign the handle
    me->runnable.handle = me;
    me->runnable.run = run;
    me->increment = 0;
    return me;
}

// Return the runnable
Runnable* Class_getRunnable(Class* me) {
    return  &me->runnable;
}

size_t Class_getCount(Class* me) {
    return me->increment;
}
