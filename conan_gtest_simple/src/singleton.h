#ifndef CONAN_CATCH_SIMPLE_SINGLETON_H
#define CONAN_CATCH_SIMPLE_SINGLETON_H

#include "runnable.h"

void Singleton_initialize();

Runnable* Singleton_getRunnable();

size_t Singleton_getCount();

#endif
