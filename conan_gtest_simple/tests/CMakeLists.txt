cmake_minimum_required(VERSION 3.1)
project(PackageTest CXX)

enable_testing()

add_executable(tests main.cpp example_test.cpp)

add_test(tests tests)
target_link_libraries(tests PRIVATE ${CONAN_LIBS} library)
