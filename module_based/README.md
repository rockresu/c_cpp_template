# Usage
```sh
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```
# Testing
Once you've built the project, you can run the tests
simply run:
```sh
ctest -VV
```
